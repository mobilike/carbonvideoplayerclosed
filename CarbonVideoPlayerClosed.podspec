#
#  Be sure to run `pod spec lint CarbonVideoPlayerClosed.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  s.name         = "CarbonVideoPlayerClosed"
  s.version      = "0.0.1"
  s.summary      = "A short description of CarbonVideoPlayerClosed. Though unmeaningful, who decides and who cares are 2 questions remaining at this point in development."
  s.description  = "A longer description of CarbonVideoPlayerClosed. There is a shorter description like 'A short description of CarbonVideoPlayerClosed. Though unmeaningful, who decides and who cares are 2 questions remaining at this point in development.' if you are interested."
  s.homepage     = "http://operamediaworks.com/"
  # s.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"
  s.license      = "MIT"
  s.author             = { "Tolga Caner" => "tolga@opera.com" }
  s.platform     = :ios
  s.ios.deployment_target = "7.1"
  s.source       = { :git => "https://bitbucket.org/mobilike/carbonvideoplayerclosed", :tag => "{s.version}" }

  s.resources = "CarbonVideoPlayer.framework/*.{png,storyboardc}"
  s.vendored_frameworks = "CarbonVideoPlayer.framework"
  s.dependency 'Google-Mobile-Ads-SDK'
  s.dependency 'GoogleAds-IMA-iOS-SDK-For-AdMob'

end
