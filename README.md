How to Use Carbon Video Player (iOS) in your applications
===================


Carbon Video Player is a standalone video player plugin that relies on Google Mobile Ads and Google Interactive Media Ads SDKs.

The player is built on [Google Media Framework](https://github.com/googleads/google-media-framework-ios) which uses AVPlayer from AVFoundation.framework as its base. The Google Media Framework (GMF) is not frequently updated, therefore Carbon Video Player is forked but is not in sync with the GMF repository since April 2016.

### Installing the library ###

**1. Using Cocoapods**

Add the next line to your pod file

```
#!ruby

pod 'CarbonVideoPlayerClosed', :git => 'git@bitbucket.org:mobilike/carbonvideoplayerclosed.git', :branch => 'master'
```

These two pods are dependencies:

*Google-Mobile-Ads-SDK
*GoogleAds-IMA-iOS-SDK-For-AdMob

If you have them in your pod file already, you should delete them. If you have "GoogleAds-IMA-iOS-SDK" as your dependency, since you need the Admob SDK (Google-Mobile-Ads-SDK), you need to remove that and get the SDK version that is designed to use with IMA SDK, namely "GoogleAds-IMA-iOS-SDK-For-AdMob", which the pod will do it for you (you don't need to add anything).

Install/Update the pods and you are set to go!

**2. Without Cocoapods**

* Download the CarbonVideoPlayer.framework from [here](https://bitbucket.org/mobilike/carbonvideoplayerclosed/get/44cc63f05e5e.zip).
* Download [GoogleAds-IMA-iOS-SDK-For-AdMob](https://dl.google.com/in-stream/google_sdk/ios/GoogleInteractiveMediaAds-ios-3.2.1.zip). Select the .framework file in "GoogleInteractiveMediaAds-GoogleIMA3ForAdMob" folder and add it to your project.
* Download [GoogleMobileAds](http://dl.google.com/googleadmobadssdk/googlemobileadssdkios.zip?hl=ja) and add it to your project
* Add "-ObjC" to Other Linker Flags in your target's **Build Settings**
* Add CarbonVideoPlayer.framework in your project's **Build Phases**' **Copy Bundle Resources** section

You should be set to go forward!

### Initializing the library ###

As soon as your app is up an running, we urge you to call:


```
#!objective-c

#import <CarbonVideoPlayer/CarbonVideoPlayer.h>

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
     [CBVideoAdvertisingManager configure];
     ...
     return YES;

}

```

or instead of the **configure** call, you can do this:

```
#!objective-c

[CBVideoAdvertisingManager configureAppWithFallbackInterstitialAdUnitId:@"YOUR FALLBACK INTERSTITIAL AD UNIT ID HERE"];
```


The configutartion call makes sure everything is right with the environment for the player to work. The fallback tag, can be passed with the appropriate configure method, or it could be set with 
```
#!objective-c

+ (void)setFallbackInterstitialAdUnitIdWithTag:(NSString*)tag;
```

just before you show a video, or anytime you want to set/change the fallback interstitial ad unit id. If you do not plan to use fallback interstitials, you can just call the **configure** method and never set the ad unit id.

### Displaying videos with DFP ads ###

The main class that should be used to create and handle a video player instance is CBVideoPlayer. If you want to display a video using CBVideoPlayer;

```
#!objective-c

#import <CarbonVideoPlayer/CarbonVideoPlayer.h>
```

Then, the following method must be used to obtain a CBVideoPlayer instance;


```
#!objective-c

+ (nullable instancetype)videoPlayerWithURL:(nonnull NSString*)url customParameters:(nullable NSDictionary*)custParams shareUrl:(nullable NSString*)shareUrl prerollTag:(nullable NSString*)prerollTag midrollTag:(nullable NSString*)midrollTag postrollTag:(nullable NSString*)postrollTag midRollPeriodInMinutes:(nullable NSNumber*)midrollPeriod completion: (void (^ __nullable)(void))completion;
```

| Parameter              | Type                | Explanation                                                                          |
|------------------------|---------------------|--------------------------------------------------------------------------------------|
| url               | NSString              | NSString is the web url of the video. This is the only parameter that is required to be valid, all of the following can be passed as nil, in which case the player will only play the video at the url without any preroll, midroll or postroll ads.                                                                     |
| customParameters          | NSDictionary              | may contain custom targeting parameters for your ads. For more information about key-value targeting, please visit [this address](https://support.google.com/dfp_premium/answer/1068325?hl=en).                                  |
| shareUrl           | NSString | The share url of the video, if any                                     |
| preRollTag             | NSString              | DFP tag for the preroll ad. Displayed before the video begins.                                                               |
| midRollTag             | NSString              | DFP tag for the midroll ad. Displayed during the video, forcefully pauses the video and plays again when the ad is finished                                                               |
| midRollPeriodInMinutes | NSNumber                 | The period for midroll ads in minutes e.g. an NSNumber with value 2 will show midroll ads after 2 minutes of playback time. Must be provided if `midRollTag` is not empty |
| postRollTag            | NSString              | DFP tag for the postroll ad. Displayed after the video ends.                                                              |
| completion            | block              | block that is called when the video is closed (either finished or manually).|

Calling this method will return you an CBVideoPlayer instance (which is a UIViewController subclass) as:


```
#!objective-c

CBVideoPlayer* videoPlayer = [CBVideoPlayer videoPlayerWithURL:@"http://www.sample-videos.com/video/mp4/480/big_buck_bunny_480p_1mb.mp4" customParameters:@{@"category":@"cartoon",@"contentId":@"123"} shareUrl:nil prerollTag:kTestPrerollTag midrollTag:kTestMidrollTag postrollTag:kTestPostrollTag midRollPeriodInMinutes:@1 completion:^{
     NSLog(@"video playback completed");
}];

```

* The url in the example above is a test url to an mp4 video. 
* The custom parameters, is a dictionary which has: "category" as key for the value "cartoon" and "contentId" as key for the value "123". There could be many more keys and values, which would serve for DFP's targeting. For more information on key-value targeting, please check [this link](https://support.google.com/dfp_sb/answer/112648?hl=en) out. 
* The "shareUrl" is set to nil in this example, since the video provided has no valid sharing options, but it could be set to the url which the video belongs to e.g. the link to the news itself. In our case, setting it to "http://www.sample-videos.com/index.php#sample-mp4-video" could make some sense.
* kTestPrerollTag, kTestMidrollTag, kTestPostrollTag are the DFP tags for your ads. Their general structure is as follows:


```
#!
https://pubads.g.doubleclick.net/gampad/ads?sz=400x300|640x480|960x540|640x360|360x200|500x300|600x480&iu=/{id}/{app}/video/{category}&ciu_szs=130x35,170x120,170x130&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&url=[referrer_url]&description_url=[description_url]&correlator=[timestamp]
```

* Some of these parameters are not mandatory for your ads to work, and there can be more parameters in your tags depending on your organisational needs.
* midRollPeriodInMinutes is set to 1 minute, which means your midroll tag will be called every minute. The framework only supports mid roll periods that are whole numbers which are greater than 0. Trying to pass NSNumbers with floats is not recommended.
* The completion block is called when the video player instance is being dismissed. This is where you should set any operation that you wish to take after the video playback.

In a nutshell, what should be done to get the player to work from a UIViewController instance is as follows:


```
#!objective-c
#import <CarbonVideoPlayer/CarbonVideoPlayer.h>

- (void) testVideo {
     __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            CBVideoPlayer* videoPlayer = [CBVideoPlayer videoPlayerWithURL:@"http://www.sample-videos.com/video/mp4/480/big_buck_bunny_480p_1mb.mp4" customParameters:@{@"category":@"cartoon",@"contentId":@"123"} shareUrl:nil prerollTag:kTestPrerollTag midrollTag:kTestMidrollTag postrollTag:kTestPostrollTag midRollPeriodInMinutes:@1 completion:^{
     NSLog(@"video playback completed");
}];
            [strongSelf presentViewController:videoPlayer animated:true completion:nil];
        }
    });
}

```

* The async call is completely optional, if you are sure that the presentation will be handled on the main thread, you don't need to call it in an async fashion.

### Fallback Interstitial ###

In case you want to display full-page interstitial ads if and when your DFP tags can not provide a valid VAST response (or simply the user is capped), the CarbonVideoPlayer offers a fallback interstitial functionality. To use it, you need to simply call this function when your application starts, or before a video player is presented:


```
#!objective-c

[CBVideoAdvertisingManager setFallbackInterstitialAdUnitIdWithTag:@"YOUR DFP INTERSTITIAL TAG HERE"];
```