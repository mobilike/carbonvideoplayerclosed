//
//  CBVideoPlayer.h
//  Pods
//
//  Created by Tolga Caner on 12/04/16.
//
//
#import <UIKit/UIKit.h>
#import <CarbonVideoPlayer/CBVideoPlayerViewController.h>
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "AdEventProtocol.h"
#import "VideoPlaybackProtocol.h"

@interface CBVideoPlayer : UIViewController <GADInterstitialDelegate, AdEventProtocol, VideoPlaybackProtocol>

/**
 The UIViewController subclass that nestles and manages the AVPlayer.
 */
@property (nonatomic,strong) CBVideoPlayerViewController* videoPlayerViewController;

/**
 The loading view that is displayed before the content begins to play.
 */
@property (unsafe_unretained, nonatomic) IBOutlet UIView *loadingView;

/**
 The companion ad view in the top left position.
 */

@property (unsafe_unretained, nonatomic) IBOutlet UIView *companionTopLeft;

/**
 The companion ad view in the top right position.
 */
@property (unsafe_unretained, nonatomic) IBOutlet UIView *companionTopRight;

/**
 The companion ad view on the bottom left position.
 */
@property (unsafe_unretained, nonatomic) IBOutlet UIView *companionBottomLeft;

/**
Returns a videoplayer instance with given parameters.
 @param url url of the video content as NSString.
 @param custParams IMA custom parameters should be put in if needed. e.g. @{@"contentId": @"1", @"category" : @"media"} etc.
 @param shareUrl shareUrl of the video (if applicable).
 @param prerollTag DFP tag of the video ad that needs to be played before the content.
 @param midrollTag DFP tag of the video ad that needs to be played during the content.
 @param postrollTag DFP tag of the video ad that needs to be played after the content.
 @param midRollPeriodInMinutes the period of the midroll ad in minutes.
 @param completion completion block that is called when the content playback is finished.
 */
+ (nullable instancetype)videoPlayerWithURL:(nonnull NSString*)url customParameters:(nullable NSDictionary*)custParams shareUrl:(nullable NSString*)shareUrl prerollTag:(nullable NSString*)prerollTag midrollTag:(nullable NSString*)midrollTag postrollTag:(nullable NSString*)postrollTag midRollPeriodInMinutes:(nullable NSNumber*)midrollPeriod completion: (void (^ __nullable)(void))completion;

/**
 **CARBON ENVIRONMENT ONLY** Returns a videoplayer instance with given parameters. **CARBON ENVIRONMENT ONLY**
 @param url url of the video content as NSString.
 @param name category name of the content as NSString (if applicable).
 @param contentId id of the content as NSString (if applicable).
 @param shareUrl shareUrl of the video (if applicable).
 */
+ (nullable instancetype)videoPlayerWithURL:(nonnull NSString*)url withCategoryName:(nullable NSString*)name contentId:(nullable NSString*)contentId shareUrl:(nullable NSString*)shareUrl;

/**
 **CARBON ENVIRONMENT ONLY** Returns a videoplayer instance with given parameters with optional video completion block **CARBON ENVIRONMENT ONLY**
 @param url url of the video content as NSString.
 @param name category name of the content as NSString (if applicable).
 @param contentId id of the content as NSString (if applicable).
 @param shareUrl shareUrl of the video (if applicable).
 @param completion completion block that is called when the content playback is finished.
 */
+ (nullable instancetype)videoPlayerWithURL:(nonnull NSString*)url withCategoryName:(nullable NSString*)name contentId:(nullable NSString*)contentId shareUrl:(nullable NSString*)shareUrl completion: (void (^ __nullable)(void))completion;

/**
 Full click action. It's not advised to call this method!
 */
- (void) adClicked;

/**
 Sets the loading view's hidden property.
 @param value YES/NO.
 */
- (void) setLoadingHidden:(BOOL)value;

/**
 Returns the IMA companion slots array (if any)
 */
-(nullable NSMutableArray*) companionSlots;

@end
